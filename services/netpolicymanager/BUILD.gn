# Copyright (c) 2021-2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/communication/netmanager_base/netmanager_base_config.gni")

config("net_policy_manager_config") {
  include_dirs = [
    "$INNERKITS_ROOT/include",
    "$INNERKITS_ROOT/netpolicyclient/include",
    "$INNERKITS_ROOT/netpolicyclient/include/proxy",
    "$INNERKITS_ROOT/netconnclient/include",
    "$NETCONNMANAGER_COMMON_DIR/include",
    "$NETMANAGER_BASE_ROOT/utils/log/include",
    "include",
    "include/stub",
    "$NETSYSCONTROLLER_ROOT_DIR/include",
    "$NETSYSNATIVE_SOURCE_DIR/include/netsys",
    "$SAMGR_MANAGER_ROOT/interfaces/innerkits/samgr_proxy/include",
  ]
}

ohos_shared_library("net_policy_manager") {
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    boundary_sanitize = true
    all_ubsan = true
    blocklist = "./policy_blocklist.txt"
    debug = false
  }

  branch_protector_ret = "pac_ret"

  sources = [
    "src/common/net_policy_event_handler.cpp",
    "src/common/net_policy_file_event_handler.cpp",
    "src/common/net_policy_service_common.cpp",
    "src/common/netsys_policy_wrapper.cpp",
    "src/core/net_policy_base.cpp",
    "src/core/net_policy_callback.cpp",
    "src/core/net_policy_core.cpp",
    "src/core/net_policy_file.cpp",
    "src/core/net_policy_firewall.cpp",
    "src/core/net_policy_rule.cpp",
    "src/core/net_policy_traffic.cpp",
    "src/firewall_rule/device_idle_firewall_rule.cpp",
    "src/firewall_rule/firewall_rule.cpp",
    "src/firewall_rule/power_save_firewall_rule.cpp",
    "src/net_policy_service.cpp",
    "src/stub/net_policy_callback_proxy.cpp",
    "src/stub/net_policy_service_stub.cpp",
  ]

  configs = [ ":net_policy_manager_config" ]

  deps = [
    "$INNERKITS_ROOT/netpolicyclient:net_policy_parcel",
    "$INNERKITS_ROOT/netstatsclient:net_stats_manager_if",
    "$NETCONNMANAGER_COMMON_DIR:net_service_common",
    "$NETMANAGER_BASE_ROOT/utils:net_manager_common",
    "$NETSYSCONTROLLER_ROOT_DIR:netsys_controller",
    "$THIRD_PARTY_ROOT/jsoncpp:jsoncpp",
  ]

  cflags = [
    "-fvisibility=hidden",
    "-fdata-sections",
    "-ffunction-sections",
    "-Os",
    "-fstack-protector-strong",
    "-D_FORTIFY_SOURCE=2",
    "-O2",
  ]

  cflags_cc = [
    "-fvisibility-inlines-hidden",
    "-Os",
    "-fstack-protector-strong",
    "-D_FORTIFY_SOURCE=2",
    "-O2",
  ]

  ldflags = [
    "-Wl,--exclude-libs=ALL",
    "-Wl,--gc-sections",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:app_manager",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hitrace:hitrace_meter",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
  ]

  defines = [
    "NETMGR_LOG_TAG = \"NetPolicyService\"",
    "LOG_DOMAIN = 0xD0015B0",
  ]

  if (enable_netmgr_debug) {
    defines += [ "NETMGR_DEBUG" ]
  }

  external_deps += [ "hilog:libhilog" ]

  part_name = "netmanager_base"
  subsystem_name = "communication"
}

ohos_static_library("net_policy_manager_static") {
  testonly = true
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    debug = false
  }

  branch_protector_ret = "pac_ret"

  sources = [
    "src/common/net_policy_event_handler.cpp",
    "src/common/net_policy_file_event_handler.cpp",
    "src/common/net_policy_service_common.cpp",
    "src/common/netsys_policy_wrapper.cpp",
    "src/core/net_policy_base.cpp",
    "src/core/net_policy_callback.cpp",
    "src/core/net_policy_core.cpp",
    "src/core/net_policy_file.cpp",
    "src/core/net_policy_firewall.cpp",
    "src/core/net_policy_rule.cpp",
    "src/core/net_policy_traffic.cpp",
    "src/firewall_rule/device_idle_firewall_rule.cpp",
    "src/firewall_rule/firewall_rule.cpp",
    "src/firewall_rule/power_save_firewall_rule.cpp",
    "src/net_policy_service.cpp",
    "src/stub/net_policy_callback_proxy.cpp",
    "src/stub/net_policy_service_stub.cpp",
  ]

  configs = [ ":net_policy_manager_config" ]

  deps = [
    "$INNERKITS_ROOT/netpolicyclient:net_policy_parcel",
    "$INNERKITS_ROOT/netstatsclient:net_stats_manager_if",
    "$NETCONNMANAGER_COMMON_DIR:net_service_common",
    "$NETMANAGER_BASE_ROOT/utils:net_manager_common",
    "$NETSYSCONTROLLER_ROOT_DIR:netsys_controller",
    "$THIRD_PARTY_ROOT/jsoncpp:jsoncpp",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:app_manager",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "eventhandler:libeventhandler",
    "hitrace:hitrace_meter",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
  ]

  defines = [
    "NETMGR_LOG_TAG = \"NetPolicyService\"",
    "LOG_DOMAIN = 0xD0015B0",
  ]

  if (enable_netmgr_debug) {
    defines += [ "NETMGR_DEBUG" ]
  }

  external_deps += [ "hilog:libhilog" ]

  part_name = "netmanager_base"
  subsystem_name = "communication"
}
